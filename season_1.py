# python basic example to print a string data


def greeting(user_name):
    return f"Hello {user_name} welcome to the python class"


if __name__ == "__main__":
    user_name = input("Hello, ¿what is your name?: ")
    print(greeting(user_name))
