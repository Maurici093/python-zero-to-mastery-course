## Python Course Notes

### Web external links

Online python editors and repos to share with a team.
- [create a repo o share code -> repl.it](https://repl.it/)
- [Many editors online -> glot.io](https://glot.io/new/python)
- [python story by Guido Van Rossum](https://www.youtube.com/watch?v=J0Aq44Pze-w)

### Class

Introduction:

- [Read diff python 2 vs 3](https://www.geeksforgeeks.org/important-differences-between-python-2-x-and-python-3-x-with-examples/)
- [More diff between python 2 and 3](https://sebastianraschka.com/Articles/2014_python_2_3_key_diff.html)
- [A basic greeting](/season_1.py)


Python Basics:

- [Data types](/data_types.py)