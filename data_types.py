# Fundamental Data Types

int # Refers to integer numbers, its can be long as you need it to be.
float # Refers to floating-point number, float values are specified with a decimal point.
bool # Boolean types may have one of two values, True or False
str # Refers to sequences of character data.

## Examples:

# checking fundamental data types in python

print(type(10)) # <class 'int'>
print(type(2/3)) # <class 'float'>
print(type("Hello World!!")) # <class 'str'>
print(type(True)) # <class 'bool'>


# Classes -> custom types

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

petter = Person("petter", 16) # peter is of type Person

# Specialized Data Types
None # refers to no value, better known as null.